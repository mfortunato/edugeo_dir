

			var gs = {
				wfs: 'http://localhost:8080/geoserver/WFS-DEMO/wfs',
				ows: 'http://localhost:8080/geoserver/WFS-DEMO/ows'
			};

			var map, user;
			var markers = [];

			// 1) CREAZIONE DI UNA CLASSE ICON (riutilizzanbile)
			var poiIcon = L.Icon.extend({
				options: {
					iconSize: [40,44],
					iconAnchor: [-20,0],
					popupAnchor: [32,-2]
				    }
			});

			// 2) CREAZIONE DELLE 3 ICON CHE DIFFERISCONO DALLA CLASSE PADRE SOLO PER 
			// "iconUrl" tutte e tre

			var chiesaIcon = new poiIcon({iconUrl:'icons/poi_chiesa.png'});
			var abbaziaIcon   = new poiIcon({iconUrl:'icons/poi_abbazia.png'});
			var cattedraleIcon  = new poiIcon({iconUrl:'icons/poi_cattedrale.png'});

			// 3) CLASSE LAYERGROUP			

			var primo = L.layerGroup([]);
			var secondo = L.layerGroup([]);
			var terzo = L.layerGroup([]);

			var group = L.featureGroup([primo, secondo, terzo]);

			// 4) CREAZIONE DEI UN LAYER DI BASE		

			var hydda_full = L.tileLayer('http://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png', {
				minZoom: 5,
				maxZoom: 18,
				attribution: 'Tiles courtesy of <a href="http://openstreetmap.se/" target="_blank">OpenStreetMap Sweden</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});

			// 5) OGGETTI CONTENETI GLI STRATI

			var overlayMaps = {
				"Protoromanico": primo,
				"Romanico Tedesco": secondo,
				"Chiese di Pellegrinaggio": terzo
			};


			// 6) Ready Document... 
			$(document).ready(function() {

				// 6.1) ASSIGN POINT TO USER
				// la "get" esegue un api che individua all'onload un array JSON

				$.get('http://jsonip.com',function(json){ 
				
					user = json;
					// 6.2) Richiama funzioni changeLeafletId e loadPoints
					changeLeafletId();
					loadPoints();
				});
				
				// 6.3) LA MAPPA
				// "L.map" inizializza la mappa nel "div" con "id='map'"
		
				map = new L.Map('map', {
					//center: [46.983333, 3.166667], 
					center: [48.22467264956519,2.9443359375],
					zoom: 5,
					layers:hydda_full,
					zoomControl: false
				});


				new L.Control.Zoom({ position: 'topright' }).addTo(map);

				// 6.4) CREAZIONE DEL LAYERS CONTROL E AGGIUNTA ALLA MAPPA DEL TUTTO

				L.control.layers(null, overlayMaps,{collapsed:false}).addTo(map);

				// 6.5) Funzione searchPopup

				$('.sea').prop('checked', false);
				$(".sea").on("click",searchPopup);
				$('#bibox').css({'opacity': '0.33', 'border-width': '0px'});
				//$("#bibox").fadeTo( "slow", 0.33 );
				//$('#caratteri').hide();

				// 6.6) DRAG & DROP
	
				$(".drag").draggable({

					// "helper": Se impostato su "clone", allora l'elemento verrà clonato e il clone sarà trascinato.
					helper: 'clone',

					// "containment": Vincola il trascinamento entro un elemento o regione determinata -"id=map"
					containment: 'map',
					
					// EVENTI del metodo ".draggable": 
					// "start": innesca qualcosa quando il trascinamento inizia.
					start: function(evt, ui) {

						$('#box').fadeTo('fast', 0.6, function() {});

					},

					// "stop": pone fine a qualcosa quando il trascinamento termina.
					stop: function(evt, ui) {

						$('#box').fadeTo('fast', 1.0, function() {});
	
						var options = {
							pid: guid(),
							type: ui.helper.attr('alt'),
							icon: eval(ui.helper.attr('alt') + 'Icon'),
							characters: null,
							draggable: true
						};

						//console.log("TYPE 1");
						//console.log(options.type);
						
						$('#name').val("");

						definePoint(map.containerPointToLatLng([ui.offset.left, ui.offset.top]),options);
						$('.0').prop('selected','selected');
						$('.linksty').prop('disabled', false);
						$('.linksty').prop('selected', false);
						$('.linkcar').prop('checked', false);
						//$('#caratteri').hide();
						$(".drag").draggable({ disabled: true });
						$('#box').css({'opacity': '0', 'border-width': '0px'});
					}
				});			
			});

			// 7) GET points
			function loadPoints() {

				var params = '?service=WFS&version=1.1.0&request=GetFeature&typeName=WFS-DEMO:points'+ '&outputFormat=json&sortBy=created+D';

				$('#upload').show();

				$.get(gs.ows + params, function(json){

					// Remove all markers
					for(i=0;i<markers.length;i++) {
						map.removeLayer(markers[i]);
					}
					markers = [];

					// Add markers
					for(i=0;i<json.features.length;i++) {
				
						var ftr = json.features[i];

						// str restituisce una stringa; split trasforma le parti della stringa divisa da un "," 
						//in elementi di un array
						var str = ftr.properties['characters'];
						var words = str.split(",");

						var options = {
							pid: ftr.properties.pid,
							name: ftr.properties.name,
							type: ftr.properties['class'],
							icon: eval(ftr.properties['class'] + 'Icon'),
							style: ftr.properties['style'],
							characters: words,
							draggable: true
						}

						//console.log("TYPE 2");
						//console.log(options.type);

						// Invertendo ftr.geometry.coordinates[1]e ftr.geometry.coordinates[0] tutto funziona
						// ma in GeoServer le coordinate restano sempre invertite
						var punto = L.marker([ftr.geometry.coordinates[1],ftr.geometry.coordinates[0]],options).addTo(map);
						var coordinate2 = [];
						coordinate2.push(ftr.geometry.coordinates[1]);
						coordinate2.push(ftr.geometry.coordinates[0]);
						createPopup(punto,coordinate2);
						// NEW
						punto.on('dblclick', function(e){
						    map.setView(e.latlng, 17);
						});

						// richiama la funzione UpdatePoint
						resetUpdate(punto);
						markers.push(punto);
					} 

					//Add alla markers on own layer
					for(i=0;i<markers.length;i++) {
						var poi = markers[i];
						var namel = markers[i].options.style;
						addMe(namel,poi);
					}
					
				}); 
				$('#upload').hide();
			}

			function resetUpdate(upoint){
				var a;
				a =upoint;
				upoint.on('dragend', function(evt){
					updatePoint(upoint);
				});
			}

			// 8) CHANGE Layer's LeafletID
			function changeLeafletId(){
				group.eachLayer(function (layer) {
					$(".linksty").each(function() {
						var cic = $(this).attr("id");
						if (layer._leaflet_id == cic){
							layer._leaflet_id= $(this).attr("value");
						}
					});
				});
				$('.leaflet-control-layers-selector').prop('checked', true);
			}

			// 0) CREATE POPUP
			function createPopup(point,coor){

				var z = (point.options.style).replace(/\-/g, ' ');
				if((point.options.characters)!=null){
					var x = (point.options.characters).join(",<br>");
					var y = (x).replace(/\-/g, ' ');
				}

				point.bindPopup('<h3 class="title">' +point.options.name+ '</h3><table  style="margin:5px"><tr><td> Tipologia: </td><td> '+point.options.type+'</td></tr><tr><td> Stile: </td><td> '+z+'</td></tr><tr><td> Caratteri: </td><td> '+y+'</td></tr><tr><td> Località: </td><td> '+ coor[0] +',<br>'+ coor[1]+'</td></tr></table><p><a class="link" onClick="deletePointBis(\'' + point.options.pid + '\');" href="#">Remove Me!</a></p>',{closeButton: false});

						//console.log("TYPE 3");
						//console.log(point.options.type);

			}

			// 9) SHOW/HIDE markers
			var spuntati=[];
			function searchPopup(){
				if ($(this).prop('checked')) {
					var itemAdd = $(this).attr("id");
					spuntati.push(itemAdd);
				}else{
					var itemRemove = $(this).attr("id");
					if((jQuery.inArray( itemRemove, spuntati))!=(-1)){
						spuntati.splice($.inArray(itemRemove, spuntati),1);
					}
				}
				searchPopupBis(spuntati);
				$( ".leaflet-control-layers-selector" ).unbind('click').click(function(){
					if($(this).prop('checked')){
						searchPopupBis(spuntati)
					}	
				});
			}

			// 9.Bis) SHOW/HIDE markers
			function searchPopupBis(spuntati){
				var spun_len = spuntati.length;
				var m;
				group.eachLayer(function (layer) {
					m = layer._map;
					if((m!=null)&&(m!=undefined)){
						for(i=0;i<markers.length;i++){
							var style = markers[i].options.style;
							if(layer._leaflet_id == style){
								if(spun_len==0){
									addMe(style,markers[i]);
								}else{
									removeMe(markers[i]);
									var char = markers[i].options.characters;
									var char_len = markers[i].options.characters.length;
									var contatore = 0;
									if(spun_len==1){
										for(j=0;j<char_len;j++){
											if(char[j]==spuntati[0]){
												j = char_len;
												addMe(style,markers[i]);
											}
										}
									}else if(spun_len>1){
										for(j=0;j<char_len;j++){
											for(k=0;k<spun_len;k++){
												if(char[j]==spuntati[k]){
													contatore++;
												}
											}
										}if(contatore==spun_len){
											addMe(style,markers[i]);
										}
									}
								}
							}
						}
					}
				});

			}

			// 10) ADD MARKER to stili's layers			
			var pointPID;
			function addMarker(namelayer,point){
				for(i=0;i<markers.length;i++) {

					if(markers[i].options.pid === point.options.pid) {

						if(pointPID==point.options.pid){
							removeMe(point);
						}
						else{
							pointPID = point.options.pid;
			
						}
						addMe(namelayer,point);
						point.closePopup();
					}
				}
			};

			// 11) Le due funzioni successive servono per attivare un evento Show/Hide, che in Leaflet viene ottenuto con 				RemoveLayer e AddLayer. (fonte: https://github.com/Leaflet/Leaflet/issues/4)

			// 11.1) DELETE/HIDE POINT FROM LAYER stili
			function removeMe(point) {
				group.eachLayer(function (layer) {
					if(layer.hasLayer(point)==true){
						layer.removeLayer(point);
					}
				});	
			}

			// 11.2) ADD/SHOW POINT TO LAYER stili
			function addMe(namelayer,point) {
				group.eachLayer(function (layer) {
					if (layer._leaflet_id == namelayer){
						layer.addLayer(point).addTo(map);
					}
				});	
			}		

			// 12) INSERT point
			function definePoint(position,options) {

				//$("#bibox").fadeTo('disabled', true);
				$('.link').css({'pointer-events': 'none'});
				$('#bibox').css({'opacity': '100', 'border-width': '3px'});

				$("#button").on("click", function(){
					alert("I campi con * sono obbligatori.");
				});
				
				var point = L.marker(position,options).addTo(map);
				var popup = L.popup()
					.setContent('<p>Compila il form a sinistra.</p><a class="link" onClick="deletePointBis(\'' + point.options.pid + '\');" href="#">Remove Me!</a></a>',{closeButton: false});
				point.bindPopup(popup).openPopup();

				// richiama funzione UpdatePOint 
				point.on('dragend', function(evt){
					updatePoint(point);
				});
				// NEW
				point.on('dblclick', function(e){
				    map.setView(e.latlng, 17);
				});

				markers.push(point);	
			
				// SELECT 1 - point.options.style

				$(".linksty").on("click", function(){

					var namelayer = $(this).attr("value");
					point.options.style = namelayer;
					addMarker(namelayer,point);
					$(".linkcar").off();
					$('.linkcar').prop('disabled', false);
					$('.linkcar').prop('checked', false);
					$(".linkcar").on("click",{point},addCar);
					//$('#caratteri').show();

				});

				// SELECT 2 - point.options.characters
				var a=[];
				var memo;
				function addCar(event) {

					if(memo!=point.options.style){
						a=[];
					}
					memo = point.options.style;

					if ($(this).prop('checked')) {
						var itemAdd = $(this).attr("value");
						a.push(itemAdd);
					}else{
						var itemRemove = $(this).attr("value");
						if((jQuery.inArray( itemRemove, a))!=(-1)){
							a.splice($.inArray(itemRemove, a),1);
						}
					}
					point.options.characters=a;

					$("#button").off();
					$("#button").on("click",{point},insertPoint);

				}

				// REQUEST WFS-T 
				// INSERT point - the button confirms selections e add xml data
				function insertPoint(event){

					var fieldValue2 = $('#name').val();
					point.options.name = fieldValue2;
					if(fieldValue2==''){
						alert("I campi con * sono obbligatori.");
					}else{
						$(".drag").draggable("enable");
						//$("#bibox").fadeTo( "slow", 0.33 );
						$('#bibox').css({'opacity': '0.33', 'border-width': '0px'});
						$('#box').css({'opacity': '100', 'border-width': '3px'});
						$("#box").prop('disabled',false);
						$('.linksty').prop('disabled', true);
						$('.linkcar').prop('disabled', true);
						$(".linksty").off();
						$(".linkcar").off();
						$("#button").off();
						//$('#caratteri').hide();

						var coordinate = [];
						coordinate.push(point.getLatLng().lat);
						coordinate.push(point.getLatLng().lng);
						createPopup(point, coordinate);

						//console.log("TYPE 4");
						//console.log(point.options.type);

						var postData = 
						'<wfs:Transaction\n'
						+ '  service="WFS"\n'
						+ '  version="1.1.0"\n'
						+ '  xmlns:WFS-DEMO="http://opengeo.org/WFS-DEMO"\n'
						+ '  xmlns:wfs="http://www.opengis.net/wfs"\n'
						+ '  xmlns:gml="http://www.opengis.net/gml"\n'
						+ '  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n'
					  	+ '  xsi:schemaLocation="http://www.opengis.net/wfs\n'
						+ '                      http://schemas.opengis.net/wfs/1.1.0/WFS-transaction.xsd\n'
						+ '                      http://localhost:8080/geoserver/WFS-DEMO/wfs/DescribeFeatureType?typename=WFS-DEMO:points">\n'				
						+ '  <wfs:Insert>\n'
						+ '    <WFS-DEMO:points>\n'
						+ '      <WFS-DEMO:pid>' + point.options.pid + '</WFS-DEMO:pid>\n'
						+ '      <WFS-DEMO:class>' + point.options.type + '</WFS-DEMO:class>\n'
						+ '      <WFS-DEMO:ip>' + user.ip + '</WFS-DEMO:ip>\n'
						+ '      <WFS-DEMO:name>' + fieldValue2 + '</WFS-DEMO:name>\n'
						+ '      <WFS-DEMO:style>' + point.options.style + '</WFS-DEMO:style>\n'
						+ '    	 <WFS-DEMO:characters>' +  point.options.characters + '</WFS-DEMO:characters>\n'
						+ '      <WFS-DEMO:the_geom>\n'
						+ '        <gml:Point srsDimension="2" srsName="urn:x-ogc:def:crs:EPSG:4326">\n'
						+ '          <gml:coordinates decimal="." cs="," ts=" ">' + point.getLatLng().lat + ',' + point.getLatLng().lng + '</gml:coordinates>\n'
						+ '        </gml:Point>\n'
						+ '      </WFS-DEMO:the_geom>\n'
						+ '    </WFS-DEMO:points>\n'
						+ '  </wfs:Insert>\n'
						+ '</wfs:Transaction>';
				
						$.ajax({
							type: "POST",
							url: gs.wfs,
							dataType: "xml",
							contentType: "text/xml",
							data: postData,
							error: function() { 
								alert("No data found."); 
							},
				        		success: function() {
				           			alert("It works!");
				        		}
						});
					}
				}
			}

			// 13) UPDATE point's coordinates
			function updatePoint(point) {

				var coordinate3 = [];
				coordinate3.push(point.getLatLng().lat);
				coordinate3.push(point.getLatLng().lng);
				createPopup(point, coordinate3);


				var postData = 
				'<wfs:Transaction\n'
				+ '  service="WFS"\n'
				+ '  version="1.1.0"\n'
				+ '  xmlns:WFS-DEMO="http://opengeo.org/WFS-DEMO"\n'
				+ '  xmlns:wfs="http://www.opengis.net/wfs"\n'
				+ '  xmlns:ogc="http://www.opengis.net/ogc"\n'
			  	+ '  xmlns:gml="http://www.opengis.net/gml"\n'
				+ '  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n'
				+ '  xsi:schemaLocation="http://www.opengis.net/wfs\n'
				+ '                      http://schemas.opengis.net/wfs/1.1.0/WFS-transaction.xsd">\n'
				+ '  <wfs:Update typeName="WFS-DEMO:points">\n'
				+ '    <wfs:Property>\n'
				+ '      <wfs:Name>the_geom</wfs:Name>\n'
				+ '      <wfs:Value>\n'
				+ '        <gml:Point srsDimension="2" srsName="urn:x-ogc:def:crs:EPSG:4326">\n'
				+ '          <gml:coordinates decimal="." cs="," ts=" ">' + point.getLatLng().lat + ',' + point.getLatLng().lng + '</gml:coordinates>\n'
				+ '        </gml:Point>\n'
				+ '      </wfs:Value>\n'
				+ '    </wfs:Property>\n'
				+ '    <ogc:Filter>\n'
				+ '      <PropertyIsEqualTo>\n'
				+ '        <PropertyName>pid</PropertyName>\n'
				+ '        <Literal>' + point.options.pid + '</Literal>\n'
				+ '      </PropertyIsEqualTo>\n'
				+ '    </ogc:Filter>\n'
				+ '  </wfs:Update>\n'
				+ '</wfs:Transaction>';
			
				$.ajax({
					type: "POST",
					url: gs.wfs,
					dataType: "xml",
					contentType: "text/xml",
					data: postData,
					//TODO: Error handling
					error: function() { 
						alert("No data found."); 
					},
                        		success: function() {
                           			alert("It works!");
                        		}
				});
			}


			// 14) DELETE point 
			function deletePointBis(pid){
				for(i=0;i<markers.length;i++) {
					if(markers[i].options.pid === pid) {
						map.removeLayer(markers[i]);
						// elimina i point se sono stati inseriti in un layer
						removeMe(markers[i]);
						markers.splice(i, 1);
						$('.0').prop('selected', false);
					}
				}
				$('.linksty').prop('disabled', true);
				$('.linkcar').prop('disabled', true);
				$(".linksty").off();
				$(".linkcar").off();
				$(".drag").draggable({ disabled: false });
				$('#bibox').css({'opacity': '0.33', 'border-width': '0px'});
				//$("#bibox").fadeTo( "slow", 0.33 );
				$('#box').css({'opacity': '100', 'border-width': '3px'});

				var postData = 
				'<wfs:Transaction\n'
			 	 + '  service="WFS"\n'
			  	 + '  version="1.1.0"\n'
				 + '  xmlns:WFS-DEMO="http://opengeo.org/WFS-DEMO"\n'
				 + '  xmlns:wfs="http://www.opengis.net/wfs"\n'
			  	 + '  xmlns:ogc="http://www.opengis.net/ogc"\n'
			  	 + '  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n'
			  	 + '  xsi:schemaLocation="http://www.opengis.net/wfs\n'
			  	 + '                      http://schemas.opengis.net/wfs/1.1.0/WFS-transaction.xsd">\n'
			  	 + '  <wfs:Delete typeName="WFS-DEMO:points">\n'
			  	 + '    <ogc:Filter>\n'
			  	 + '      <PropertyIsEqualTo>\n'
			  	 + '        <PropertyName>pid</PropertyName>\n'
			  	 + '        <Literal>' + pid + '</Literal>\n'
			  	 + '      </PropertyIsEqualTo>\n'
			  	 + '    </ogc:Filter>\n'
			  	 + '  </wfs:Delete>\n'
			  	 + '</wfs:Transaction>';
			  
				$.ajax({
					type: "POST",
					url: gs.wfs,
					dataType: "xml",
					contentType: "text/xml",
					data: postData,	
					//TODO: Error handling
					error: function() { 
						alert("No data found."); 
					},
                        		success: function() {
                           			alert("It works!");
                        		}
				});

			}

		// Create a GUID
			function S4() {
				return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
			}
			function guid() {
				return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
			}


	
