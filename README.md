﻿edugeo
======

The **edugeo_dir** repository has been realized for educational purposes with two Docker images, which run respectively a GeoServer and PostgreSQL/PostGIS container. They put together support and testing software for a personalized demo of web mapping. The use of the application requires the simultaneous run of two containers created with the reference image. The images are published on a Docker Hub repository ([link](https://hub.docker.com/r/mfortunato/)).


To build the application `geoserver/www/demo`:

1. download the directory "edugeo_dir" from Bitbucket with `git clone https://bitbucket.org/mfortunato/edugeo_dir`;

2. from the shell, run the commands 
`cd edugeo_dir/edugeo` and then
`docker-compose up -d`;

3. type in the browser address bar `localhost:8080/geoserver/www/demo/mapindex.html`;

4. Enjoy it!


BRIEF DESCRIPTION
-----------------

The files in edugeo_dir repository create end run the complete suite of tools, which build the application. The repository contains:

* a file .yml called `docker-compose.yml`
* and a folder called `WWW`.


The **docker-compose.yml** is a definition file of services that make up a Docker multi-container application. It simplify in a single step the downloading of images, the creation of the two containers, their connection and their start-up. The file must be launched with the tool Docker Compose ([link](https://docs.docker.com/compose)).

The **WWW** folder contains files and library of browser interface application. Between the instructions defined in the `docker-compose.yml`, there is the option "`volumes`" that connects the `www` folder to the `/opt/geoserver/data_dir/www/` position, in the "`geoserver`" container. So `www` becomes a shared folder between the local host and the container. 


For more information about the application, see this [link](https://hub.docker.com/r/mfortunato/geoserver).